<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('board_id')->unsigned()->index();
            $table->longText('slug')->index();
            $table->string('name', 300);
            $table->string('description', 400);
            $table->string('checklist', 400);
            $table->string('type', 100);
            $table->smallInteger('status');
            $table->string('assignee', 100);
            $table->string('label', 100);
            $table->string('priority', 100);
            $table->string('sprint', 100);
            $table->unsignedBigInteger('original_estimate');
            $table->unsignedBigInteger('time_spent');
            $table->smallInteger('story_points');
            $table->unsignedBigInteger('time_remaining');
            $table->string('comments', 400);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
