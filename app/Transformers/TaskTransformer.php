<?php 

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Repositories\Task;

class TaskTransformer extends TransformerAbstract {
  
  public function transform(Task $task) {
    return [
      'name' => $task->name,
      'slug' => $task->slug,
      'board_id' => $task->board_id,
      'description' => $task->description,
      'checklist' => $task->checklist,
      'type' => $task->type,
      'status' => $task->status,
      'assignee' => $task->assignee,
      'label' => $task->label,
      'priority' => $task->priority,
      'sprint' => $task->sprint,
      'original_estimate' => $task->original_estimate,
      'time_spent' => $task->time_spent,
      'story_points' => $task->story_points,
      'time_remaining' => $task->time_remaining,
      'comments' => $task->comments,
      'created_by' => $task->created_by,
      'created_at' => $task->created_at,
      'updated_at' => $task->updated_at,
    ];
  }

}