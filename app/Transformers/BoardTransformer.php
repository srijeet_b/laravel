<?php 

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Repositories\Board;

class BoardTransformer extends TransformerAbstract {
  
  public function transform(Board $board) {
    return [
      'slug' => $board->slug,
      'name' => $board->name,
      'type' => $board->type,
      'status' => $board->status,
      'tasks_count' => $tasks_count, 
      'created_by' => $board->created_by,
      'managed_by' => $board->managed_by,
      'created_at' => $board->created_at,
      'updated_at' => $board->updated_at,
    ];
  }

}