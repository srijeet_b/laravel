<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    const NEW_TASK = 1;
    const IN_PROGRESS = 2;
    const READY_FOR_REVIEW = 3;
    const WAITING_FOR_DEPLOYMENT = 4;
    const DEPLOYED = 5;
    const COMPLETED = 6;

    const TASK_NAMES = [
    self::NEW_TASK => 'NEW TASK',
    self::IN_PROGRESS => 'IN PROGRESS',
    self::READY_FOR_REVIEW => 'READY FOR REVIEW',
    self::WAITING_FOR_DEPLOYMENT => 'WAITING FOR DEPLOYMENT',
    self::DEPLOYED => 'DEPLOYED',
    self::COMPLETED => 'COMPLETED',
    ];

    protected static $unguarded = true;
    protected $table = 'tasks';
    protected $fillable = ['*'];
    protected $encryptable = [
    //task details
    ];
    public function board() {
    return $this->belongsTo('Repositories\Board', 'board_id');
    }
}
