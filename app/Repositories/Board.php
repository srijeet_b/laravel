<?php 

namespace App\Repositories;


class Board {
  const SPRINT = 1;
  const KANBAN = 2;

  const BOARD_NAMES = [
    self::SPRINT => 'SPRINT',
    self::KANBAN => 'KANBAN',
  ];

  protected static $unguarded = true;
  protected $table = 'boards';
  protected $fillable = ['*'];
  protected $encryptable = [
    //board details
  ];

  public function tasks() {
    return $this->hasMany('Repositories\Task', 'board_id', 'id');
  }
}