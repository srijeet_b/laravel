<?php 

namespace Repositories;


class User {
  protected static $unguarded = true;
  protected $table = 'boards';
  protected $fillable = ['email', 'name', 'customer_referred_id', 'customer_phone', 'organisation'];
}