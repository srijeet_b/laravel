<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Board;
use App\Transformers\BoardTransformer;

class BoardsController extends Controller
{
    public function index(Request $request) {
        if($request->has('slug')) {
            $board = Board::where('slug', $request->slug)->first();
            return response()->eloquent($board)->setTransformer(new BoardTransformer())->toJson()->setStatusCode(200);
        }
        else {
            $boards = Board::where('company_id', $request->company_id)->all();
            return response()->eloquent($board)->setTransformer(new BoardTransformer())->toJson()->setStatusCode(200);
        }
        return response()->json('Invalid ! Board doesn\'t exist', 400);
    }
    
    public function store(Request $request) {
        $boardalreadyexists = Board::where('slug', $request->slug)->first();
        if(empty($boardalreadyexists)) {
            return response()->json(['message' => 'Board already exists' ], 400);
        }
        $newtask = Board::create([
            'slug' => $request->slug,
            'name' => $request->name,
            'description' => $request->description ?? '',
            'checklist' => $request->checklist ?? '',
            'type' => $request->type,
            'status' => $request->status,
            'assignee' => $request->assignee,
            'label' => $request->label,
            'priority' => $request->priority ?? 'medium',
            'sprint' => $request->sprint ?? '',
            'original_estimate' => $request->original_estimate ?? 0,
            'time_spent' => $request->timespent ?? 0,
            'story_points' => $request->story_point ?? 0,
            'time_remaining' => $request->time_remaining ?? null,
            'comments' => $request->comments ?? '',
        ]);
        return response()->json(['message' => 'Data Inserted successfully', 'task_id' => $board->id ], 200);
    }

    public function update(Request $request) {
        $board = Board::where('slug', $request->slug)->first();
        if(!empty($board)) {
            $board->name = $request->name;
            $board->description = $request->description;
            $board->checklist = $request->checklist;
            $board->type = $request->type;
            $board->status = $request->status;
            $board->assignee = $request->assignee;
            $board->label = $request->label;
            $board->priority = $request->priority;
            $board->sprint = $request->sprint;
            $board->original_estimate = $request->original_estimate;
            $board->time_spent = $request->time_spent;
            $board->story_points = $request->story_porint;
            $board->time_remaining = $request->time_remaining;
            $board->comments = $request->comments;
            $board->save();
        }
        else {
            return response()->json(['message' => 'Board doesn\'t exist'], 400);
        }
    }

    public function flush(Request $request) {
        $boardalreadyexists =Board::where('slug', $request->slug)->first();
        if(!empty($boardalreadyexists)) {
            Board::delete('id', $boardalreadyexists->id);
            return response()->json(['message' => 'Board successfully deleted!', 'slug' => $request->slug, 'task_id' => $boardalreadyexists->id ], 200);
        }
        else {
            return response()->json(['message' => 'Board doesn\'t exist'], 400);
        }
    }
}

