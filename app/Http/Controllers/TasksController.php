<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Task;
use App\Transformers\TaskTransformer;

class TasksController extends Controller
{

    public function index(Request $request) {
        if($request->has('slug')) {
            $task = Task::where('slug', $request->slug)->first();
            return response()->eloquent($task)->setTransformer(new TaskTransformer())->toJson()->setStatusCode(200);
        }
        else {
            $tasks = Task::where('company_id', $request->company_id)->all();
            return response()->eloquent($tasks)->setTransformer(new TaskTransformer())->toJson()->setStatusCode(200);
        }
        return response()->json(['message' => 'Invalid ! Task doesn\'t exist'],400);
    }
    
    public function store(Request $request) {
        $taskalreadyexists = Task::where('slug', $request->slug)->first();
        if(empty($taskalreadyexists)) {
            return response()->json(['message' => errorBadRequest('Task already exists')], 400);
        }
        $newtask = Task::create([
            'slug' => $request->slug,
            'name' => $request->name,
            'description' => $request->description ?? '',
            'checklist' => $request->checklist ?? '',
            'type' => $request->type,
            'status' => $request->status,
            'assignee' => $request->assignee,
            'label' => $request->label,
            'priority' => $request->priority ?? 'medium',
            'sprint' => $request->sprint ?? '',
            'original_estimate' => $request->original_estimate ?? 0,
            'time_spent' => $request->timespent ?? 0,
            'story_points' => $request->story_point ?? 0,
            'time_remaining' => $request->time_remaining ?? null,
            'comments' => $request->comments ?? '',
        ]);
        return $this->response(['message' => 'Data Inserted successfully', 'task_id' => $task->id ], 200);
    }

    public function update(Request $request) {
        $task = Task::where('slug', $request->slug)->first();
        if(!empty($task)) {
            $task->name = $request->name;
            $task->description = $request->description;
            $task->checklist = $request->checklist;
            $task->type = $request->type;
            $task->status = $request->status;
            $task->assignee = $request->assignee;
            $task->label = $request->label;
            $task->priority = $request->priority;
            $task->sprint = $request->sprint;
            $task->original_estimate = $request->original_estimate;
            $task->time_spent = $request->time_spent;
            $task->story_points = $request->story_porint;
            $task->time_remaining = $request->time_remaining;
            $task->comments = $request->comments;
            $task->save();
        }
        else {
            return response()->json(['message' => 'Task doesn\'t exist'], 400);
        }
    }

    public function flush(Request $request) {
        $taskalreadyexists =Task::where('slug', $request->slug)->first();
        if(!empty($taskalreadyexists)) {
            Task::delete('id', $taskalreadyexists->id);
            return response()->json(['message' => 'Task successfully deleted!', 'slug' => $request->slug, 'task_id' => $taskalreadyexists->id ], 200);
        }
        else {
            return response()->json(['message' => 'Task doesn\'t exist'], 400);
        }
    }
}

