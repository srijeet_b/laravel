<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Validator;
use Repositories\Task;

class Tasks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->type == "POST") {
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'slug' => 'required|string',
            'description' => 'string',
            'checklist' => 'string',
            'type' => 'required|string',
            'status' => 'required|numeric|max:1|min:0',
            'assignee' => 'required|string',
            'label' => 'required|numeric|',
            'priority' => 'required|string',
            'sprint' => 'string',
            'original_estimate' => 'numeric|min:0',
            'time_spent' => 'numeric|min:0',
            'story_points' => 'numeric|min:0',
            'time_remaining' => 'numeric|min:0',
            'comments' => 'string',
        ]);
        if($validator->fails()) {
            return response()->json(['message' => 'Invalid Payload !', 'errors' => $validator->errors() ], 400);
        }
        return $next($request);
    }
}
