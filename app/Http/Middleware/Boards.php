<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Validator;
use Repositories\Board;
use Illuminate\Validation\Rule;

class Boards
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $rules = [
        'slug' => 'required',
        'name' => 'required',
        'type' => ['required', Rule::in(['kanban', 'sprint'])],
        ];
        if($request->isMethod('post')) {
            $rules['status'] = 'required';
        }
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json(['message' => 'Invalid Payload !', 'errors' => $validator->errors() ], 400);
        }
        return $next($request);
    }
}
