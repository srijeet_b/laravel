Changes in - 

Controller (App\Http\Controllers)
1. TasksController
2. BoardsController

Models (App\Models)
1. Task
2. Board

Middleware (App\Http\Middleware)
1. Task
2. Middleware 
3. Kernel.php (App\Http)