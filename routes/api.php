<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$prefix = null;
Route::group(['namespace' => 'App\Http\Controllers'], function() {
    Route::get('/test', 'BoardsController@test');
    Route::group(['prefix' => 'auth', ], function() {
        // Route::get('login', 'AuthController@login');
        // Route::post('signup', 'AuthController@signup');
    });
    Route::group(['prefix' => 'boards'], function() {
        Route::group(['middleware' => 'boards.middle'], function() {
            Route::get('/', 'BoardsController@index');
            Route::post('/', 'BoardsController@store');
        });
        Route::patch('/', 'BoardsController@update');
        Route::delete('/', 'BoardsController@flush');
    });
    Route::group(['prefix' => 'tasks', 'middleware' => 'auth'], function() {
        Route::group(['middleware' => 'tasks.middle'], function() {
            Route::get('/', 'TasksController@index');
            Route::post('/', 'TasksController@store');
        });
        Route::patch('/', 'TasksController@update');
        Route::delete('/', 'TasksController@flush');
    });
});    
